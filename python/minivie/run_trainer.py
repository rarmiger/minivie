#!/usr/bin/env python
# test script for MPL interface
#
# This test function is intended to be operated from the command line to bring up a short menu to allow basic
# training and operation of the MPL
#
# Revisions:
# 2016OCT05 Armiger: Created
# 2023OCT14 Armiger: Updated to function with latest MiniVIE

import time
import numpy as np
import sys

################################################
# Setup user config.  This is especially needed by the Plant to get joint limits
################################################
from utilities import user_config
user_config.read_user_config_file('vmpl_user_config.xml')

# Setup logging.  This will create a log file like: USER_2016-02-11_11-28-21.log to which all 'logging' calls go
user_config.setup_file_logging(log_level='ERROR') # ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']

################################################
# Configure Signal Source
################################################
from inputs.myo import myo_client

num_samples = 70  # This number of samples will be used for feature extraction

myo1 = myo_client.MyoUdp(local_addr_str='//0.0.0.0:15001', remote_addr_str='//127.0.0.1:16001', num_samples=num_samples)
myo2 = myo_client.MyoUdp(local_addr_str='//0.0.0.0:15002', remote_addr_str='//127.0.0.1:16002', num_samples=num_samples)
myo1.connect()
myo2.connect()

# Make some test calls for debug
d1 = myo1.get_data()
myo1.get_data_rate_emg()

d2 = myo2.get_data()
myo2.get_data_rate_emg()

SignalSource = [myo1, myo2]
num_devices = len(SignalSource)
num_channels = sum(src.num_channels for src in SignalSource)

data = np.concatenate([s.get_data() for s in SignalSource], axis=1)

# Server can be started with:
# python3 -m inputs.myo.myo_sim --SIM_UNIX --ADDRESS //127.0.0.1:15001
# python3 -m inputs.myo.myo_server_bleak -x myo.xml

################################################
# Configure Feature Extraction
################################################
import pattern_rec.features
data_scale_factor = 0.01
sample_rate = 200

Mav = pattern_rec.features.Mav()
Len = pattern_rec.features.CurveLen()
Zc = pattern_rec.features.Zc(fs=sample_rate, zc_thresh=0.5)
Ssc = pattern_rec.features.Ssc(fs=sample_rate, ssc_thresh=0.5)

Features = [Mav, Len, Zc, Ssc]
num_features = len(Features)

f = np.empty((num_channels*num_features,), dtype=np.float64)
f[0::num_features] = Mav.extract_features(data*data_scale_factor)
f[1::num_features] = Len.extract_features(data*data_scale_factor)
f[2::num_features] = Zc.extract_features(data*data_scale_factor)
f[3::num_features] = Ssc.extract_features(data*data_scale_factor)

# For console debugging
print_feature_id = 0

################################################
# Configure Training Data Manager
################################################
import pattern_rec.training_data
TrainingData = pattern_rec.training_data.TrainingData()
TrainingData.load()
TrainingData.num_channels = num_channels

################################################
# Configure Classifier
################################################
import pattern_rec.classifier
SignalClassifier = pattern_rec.classifier.Classifier(TrainingData)
SignalClassifier.fit()

# setup majority vote buffer
from collections import Counter, deque
decision_buffer = deque([], 9)

################################################
# Configure Output
################################################
from mpl.open_nfu.open_nfu_sink import NfuSink
DataSink = NfuSink(local_addr_str='//0.0.0.0:9028', remote_addr_str='//127.0.0.1:9027')
DataSink.mpl_connection_check = True

# from mpl.unity import UnityUdp
# DataSink = UnityUdp(local_addr_str='//0.0.0.0:25001', remote_addr_str='//127.0.0.1:25000')

DataSink.connect()

# NFU Simulator:
# python3 -c "from mpl.open_nfu.open_nfu_sim import Simulator;
# a = Simulator(local_address=('0.0.0.0', 9027), remote_address=('127.0.0.1', 9028)); a.start()"

################################################
# Configure 'Plant' model to hold system state
################################################
import controls.plant
dt = 0.02
# Plant maintains current limb state (positions) during velocity control
# filename = 'mpl/#VMPL_ROC.xml'
filename = 'mpl/#MPL_GEN3_ROC.xml'
Plant = controls.plant.Plant(dt, filename)
hand_gain_value = 1.0
gain_value = 1.0

if isinstance(DataSink, NfuSink) and DataSink.mpl_connection_check:

    print('Checking for valid percepts...')
    wait_time_ms = 200
    n = 0
    while not DataSink.data_received():
        time.sleep(wait_time_ms / 1000)
        print(f'Waiting {wait_time_ms} ms for valid percepts...')

    # After loop ends, synchronize joint positions
    for i in range(0, len(Plant.joint_position)):
        Plant.joint_position[i] = DataSink.position['last_percept'][i]


################################################
# Start Menu
################################################
# total arguments
n = len(sys.argv)
print("Total arguments passed:", n)
print(sys.argv)
auto_run = n > 1
choice = 'R'

while True:
    # Show menu
    print(8 * '\n')
    print(30 * '-')
    print("   T R A I N E R  ")
    print(30 * '-')
    print(" P. Preview Data Sources")
    print(" R. Run decode")
    print(" B. Backup training data")
    print(" S. Save File")
    print(" X. Reset File")
    print(' M. Get MPL Status')
    print(30 * '-')
    for idx, val in enumerate(TrainingData.motion_names):
        stored_samples = TrainingData.get_totals(idx)
        if stored_samples > 0:
            print(f"{idx+1:2d}. {val} [{stored_samples}]")
        else:
            print(f"{idx + 1:2d}. {val}")
    print(30 * '-')
    print(" 0. Exit")
    print(30 * '-')

    if not auto_run:
        # Get input
        choice = input('Enter selection : ')
        assert isinstance(choice, str)  # native str on Py2 and Py3

    # Take action as per selected menu-option #
    if choice == '0':
        # exit case
        print("Exiting...")
        break
    elif choice.upper() == 'M':
        while True:
            try:
                print(DataSink.get_status_msg().replace('<br>', ' | '), end='\r')
            except KeyboardInterrupt:
                print('Stopping')
                break

    elif choice.upper() == 'P':
        # Preview data stream
        while True:
            try:
                time.sleep(dt)  # 50Hz

                data = np.concatenate([s.get_data() * data_scale_factor for s in SignalSource], axis=1)
                f = np.empty((num_channels * num_features,), dtype=np.float64)
                for i, feature in enumerate(Features):
                    f[i::num_features] = feature.extract_features(data)

                print(''.join(format(s.get_data_rate_emg(), "6.1f") for s in SignalSource) + ' |' +
                      ''.join(format(x, "6.2f") for x in f[print_feature_id::num_features]), end='\r')

            except KeyboardInterrupt:
                print('Stopping')
                break

    elif choice.upper() == 'S':
        # save
        TrainingData.save()
        SignalClassifier.fit()

    elif choice.upper() == 'B':
        # backup
        TrainingData.copy()

    elif choice.upper() == 'X':
        # clear
        TrainingData.reset()
        SignalClassifier.fit()

    elif choice.upper() == 'R':
        # run classifier:

        # initialize output
        output = {'status': 'RUNNING', 'features': None, 'decision': 'None', 'vote': None}

        last_decision = None

        loop_count = 0

        while True:
            try:
                # Fixed rate loop.  get start time, run model, get end time; delay for duration
                time_begin = time.perf_counter()

                loop_count += 1
                if loop_count > 500:
                    loop_count = 0
                    print('\n' + DataSink.get_status_msg().replace('<br>', ' | '))

                # Get features from emg data
                data = np.concatenate([s.get_data() * data_scale_factor for s in SignalSource], axis=1)
                f = np.empty((num_channels * num_features,), dtype=np.float64)
                for i, feature in enumerate(Features):
                    f[i::num_features] = feature.extract_features(data)

                # classify
                # format the data in a way that sklearn wants it
                feature_learn = f.reshape(1, -1)
                decision_id, output['status'] = SignalClassifier.predict(feature_learn)
                if decision_id is None:
                    continue

                # perform majority vote
                decision_buffer.append(decision_id)
                counter = Counter(decision_buffer)

                if TrainingData.motion_names[decision_id] != 'No Movement':
                    # Immediately stop if class is no movement, otherwise use majority vote
                    decision_id = counter.most_common(1)[0][0]

                # get decision name
                class_decision = TrainingData.motion_names[decision_id]
                output['decision'] = class_decision

                if class_decision != last_decision:
                    print(f'\nNew Class Decision: {class_decision}')
                    last_decision = class_decision

                # parse decision type as arm, grasp, etc
                class_info = controls.plant.class_map(class_decision)

                # Set joint velocities
                Plant.new_step()

                # set the mapped class into either a hand or arm motion
                if class_info['IsGrasp']:
                    # the motion class is either a grasp type or hand open
                    if class_info['GraspId'] is not None and Plant.grasp_position < 0.2:
                        # change the grasp state if still early in the grasp motion
                        Plant.grasp_id = class_info['GraspId']
                    Plant.set_grasp_velocity(class_info['Direction'] * hand_gain_value)

                if not class_info['IsGrasp']:
                    # the motion class is an arm movement
                    Plant.set_joint_velocity(class_info['JointId'], class_info['Direction'] * gain_value)

                # update positions
                Plant.update()

                # transmit output
                DataSink.send_joint_angles(Plant.joint_position, Plant.joint_velocity)

                print(''.join(format(x, "6.1f") for x in Plant.joint_position), end='\r')

                time_end = time.perf_counter()
                time_elapsed = time_end - time_begin
                if dt > time_elapsed:
                    time.sleep(dt - time_elapsed)
                else:
                    print("Timing Overload: {}".format(time_elapsed))

            except KeyboardInterrupt:
                print('Stopping')
                choice = '0'
                break

    else:
        # Train the selected class
        try:
            class_id = int(choice) - 1
        except ValueError:
            print('Invalid Selection')
            continue

        if class_id < 0 or class_id >= len(TrainingData.motion_names):
            print('Selection Out of Range')
            continue

        for _ in range(100):
            time.sleep(dt)

            # Get features from emg data
            data = np.concatenate([s.get_data() * data_scale_factor for s in SignalSource], axis=1)
            f = np.empty((num_channels * num_features,), dtype=np.float64)
            for i, feature in enumerate(Features):
                f[i::num_features] = feature.extract_features(data)

            TrainingData.add_data(f, class_id, TrainingData.motion_names[class_id])

            print(TrainingData.motion_names[class_id] +
                  ''.join(format(x, "6.2f") for x in f[print_feature_id::num_features]), end='\r')

        # save
        TrainingData.save()

        SignalClassifier.fit()
        pass

DataSink.close()
myo2.close()
myo1.close()

print("Done")
