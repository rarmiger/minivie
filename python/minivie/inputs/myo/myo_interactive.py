# To discover Bluetooth devices that can be connected to:

import asyncio
from bleak import BleakScanner
# Connect to a Bluetooth device and read its model number and properties, vibrate, then shut off:
#
# Shut off myo:
#   py demo_myo_interface.py MAC:AD:DR:ESS

import sys
import asyncio
from bleak import BleakClient
from myo_server_bleak import MyoBluetoothInterface
import struct

myo = MyoBluetoothInterface()
scan_timeout = 5.0


async def shutdown(address):
    async with BleakClient(address, timeout=scan_timeout) as client:
        # Get Firmware Version
        result = await client.read_gatt_char(myo.firmware_version_characteristic)
        version = struct.unpack('<HHHH', result)
        print(f"Firmware Version: Major.Minor.Patch {version[0]}.{version[1]}.{version[2]}  Hardware Rev{version[3]}")

        # Turn off sleep
        print('Turning off sleep mode')
        await client.write_gatt_char(myo.command_characteristic,
                                     bytearray([myo.command_set_sleep_mode, 1, myo.sleep_mode_never_sleep]))

        # vibrate_myo
        print('Vibrating')
        await client.write_gatt_char(myo.command_characteristic,
                                     bytearray([myo.command_vibrate, 1, myo.vibration_long]))

        print('Waiting...')
        await asyncio.sleep(1)

        # deep sleep
        print('Sending myo to deep sleep')
        await client.write_gatt_char(myo.command_characteristic, bytearray([myo.command_deep_sleep, 0]))


async def func_scan():

    devices = await BleakScanner.discover(timeout=scan_timeout, return_adv=True)

    myos = []
    for d in devices:
        adv_data = devices[d][1]
        if 'D5060001-A904-DEB9-4748-2C7F4A124842'.lower() in adv_data.service_uuids:
            print(f'Myo (RSSI={adv_data.rssi}): {adv_data.local_name}')
            myos.append(d)
        else:
            print(f'BLE (RSSI={adv_data.rssi}): {adv_data.local_name}')

    return devices, myos


async def main():
    print(f'Scanning for {scan_timeout} seconds for ANY BLE Device...')

    # create and schedule the task
    task = asyncio.create_task(func_scan())

    # wait for the task to complete
    await task
    task_result = task.result()

    # report a final message
    print('\nScan Complete.  MYO Devices:')
    all_devices = task_result[0]
    myos = task_result[1]

    if not len(myos):
        print('No MYO devices found')
        return

    while True:

        # Show menu
        print(8 * '\n')
        print(50 * '-')
        print("   Select Myo Device to SHUTDOWN  ")
        print(50 * '-')
        print(50 * '-')
        for idx, val in enumerate(myos):
            print(f"{idx+1:2d}. {val}")
        print(50 * '-')
        print(" 0. Exit")
        print(50 * '-')

        # Get input
        choice = input('Enter selection : ')
        assert isinstance(choice, str)  # native str on Py2 and Py3

        # Take action as per selected menu-option #
        if choice == '0':
            # exit case
            print("Exiting...")
            break
        else:
            # Train the selected class
            try:
                myo_id = int(choice)
            except ValueError:
                print('Invalid Selection')
                continue

            if myo_id < 1 or myo_id > len(myos):
                print('Selection Out of Range')
                continue

            await shutdown(myos[myo_id-1])
            await asyncio.sleep(scan_timeout+1)

        for m in myos:
            print(all_devices[m])

# start the asyncio program
asyncio.run(main())
