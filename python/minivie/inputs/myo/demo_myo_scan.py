# To discover Bluetooth devices that can be connected to:

import asyncio
from bleak import BleakScanner

scan_timeout = 5.0


async def func_scan():

    devices = await BleakScanner.discover(timeout=scan_timeout, return_adv=True)

    myos = []
    for d in devices:
        adv_data = devices[d][1]
        if 'D5060001-A904-DEB9-4748-2C7F4A124842'.lower() in adv_data.service_uuids:
            print(f'Myo (RSSI={adv_data.rssi}): {adv_data.local_name}')
            myos.append(d)
        else:
            print(f'BLE (RSSI={adv_data.rssi}): {adv_data.local_name}')

    return devices, myos


async def main():
    print(f'Scanning for {scan_timeout} seconds for ANY BLE Device...')

    # create and schedule the task
    task = asyncio.create_task(func_scan())

    # wait for the task to complete
    await task
    task_result = task.result()

    # report a final message
    print('\nScan Complete.  MYO Devices:')
    all_devices = task_result[0]
    myos = task_result[1]
    for m in myos:
        print(all_devices[m])

# start the asyncio program
asyncio.run(main())
